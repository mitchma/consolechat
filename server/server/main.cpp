#include <iostream>
#include <WS2tcpip.h>

#pragma comment (lib, "ws2_32.lib")

const int DEFAULT_PORT = 54000;
const int DEFAULT_BUFLEN = 4096;

int main()
{
	// initialize winsock
	WSADATA wsaData;
	WORD ver = MAKEWORD(2, 2);
	int result = WSAStartup(ver, &wsaData);
	if (result != NO_ERROR)
	{
		fprintf(stderr, "Cannot initialize winsock\n");
		return 1;
	}

	// create socket
	SOCKET listener = socket(AF_INET, SOCK_STREAM, 0);
	if (listener == INVALID_SOCKET)
	{
		fprintf(stderr, "Cannot create socket: %u\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	// bind IP and port
	sockaddr_in descriptor; // sockaddr_in is IP-based descriptor
	descriptor.sin_family = AF_INET;
	descriptor.sin_port = htons(DEFAULT_PORT); // network is big endian, Windows is little endian
	descriptor.sin_addr.S_un.S_addr = INADDR_ANY;

	result = bind(listener, (sockaddr *)&descriptor, sizeof(descriptor)); // sockaddr is generic descriptor
	if (result == SOCKET_ERROR)
	{
		fprintf(stderr, "Cannot bind socket with IP and port: %u\n", WSAGetLastError());
		closesocket(listener);
		WSACleanup();
		return 1;
	}

	// listen on socket
	listen(listener, SOMAXCONN);

	// wait for connection
	sockaddr_in client;
	int clientSize = sizeof(client);
	SOCKET acceptor = accept(listener, (sockaddr *)&client, &clientSize);
	if (acceptor == INVALID_SOCKET)
	{
		fprintf(stderr, "Cannot accept listen socket: %u\n", WSAGetLastError());
		closesocket(listener);
		WSACleanup();
		return 1;
	}

	char host[NI_MAXHOST]; // client's remote name
	char service[NI_MAXSERV]; // service client is connected to

	// memset(host, 0, NI_MAXHOST);
	ZeroMemory(host, NI_MAXHOST);
	ZeroMemory(service, NI_MAXSERV);

	if (getnameinfo((sockaddr *)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		printf("%s connected on port %s\n", host, service);
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		printf("%s connected on port %hu\n", host, ntohs(client.sin_port));
	}

	// close listening socket
	closesocket(listener);

	// accept and echo message
	const int bufferSize = DEFAULT_BUFLEN;
	char recvBuffer[bufferSize];

	while (true)
	{
		ZeroMemory(recvBuffer, bufferSize);

		// wait for client data
		result = recv(acceptor, recvBuffer, bufferSize, 0);
		if (result == SOCKET_ERROR)
		{
			fprintf(stderr, "Error in recv(): %d\n", WSAGetLastError());
			break;
		}
		else if (result == 0)
		{
			printf("Connection closed\n");
			break;
		}

		// echo message back
		send(acceptor, recvBuffer, result + 1, 0); // size + 1 due to terminating 0
	}

	// cleanup
	closesocket(acceptor);
	WSACleanup();
}
