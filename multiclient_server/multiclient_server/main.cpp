#include <iostream>
#include <WS2tcpip.h>
#include <sstream>

#pragma comment (lib, "ws2_32.lib")

const int DEFAULT_PORT = 54000;
const int DEFAULT_BUFLEN = 4096;

int main()
{
	// initialize winsock
	WSADATA wsaData;
	WORD ver = MAKEWORD(2, 2);
	int result = WSAStartup(ver, &wsaData);
	if (result != NO_ERROR)
	{
		fprintf(stderr, "Cannot initialize winsock\n");
		return 1;
	}

	// create socket
	SOCKET listener = socket(AF_INET, SOCK_STREAM, 0);
	if (listener == INVALID_SOCKET)
	{
		fprintf(stderr, "Cannot create socket: %u\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	// bind IP and port
	sockaddr_in descriptor; // sockaddr_in is IP-based descriptor
	descriptor.sin_family = AF_INET;
	descriptor.sin_port = htons(DEFAULT_PORT); // network is big endian, Windows is little endian
	descriptor.sin_addr.S_un.S_addr = INADDR_ANY;

	result = bind(listener, (sockaddr *)&descriptor, sizeof(descriptor)); // sockaddr is generic descriptor
	if (result == SOCKET_ERROR)
	{
		fprintf(stderr, "Cannot bind socket with IP and port: %u\n", WSAGetLastError());
		closesocket(listener);
		WSACleanup();
		return 1;
	}

	// listen on socket
	listen(listener, SOMAXCONN);

	// multiple connection handling
	fd_set master;
	FD_ZERO(&master);

	// add listening socket to set
	FD_SET(listener, &master);

	while (true)
	{
		fd_set copy = master;
		int socketCount = select(0, &copy, nullptr, nullptr, nullptr);
		for (int i = 0; i < socketCount; i++)
		{
			SOCKET sock = copy.fd_array[i];

			// accept new connection
			if (sock == listener)
			{
				// accept new connection
				SOCKET client = accept(listener, nullptr, nullptr);

				// add new connection to list of connected clients
				FD_SET(client, &master);

				// send welcome message to new client
				std::string msg = "Welcome to the server\n";
				send(client, msg.c_str(), msg.size() + 1, 0);
			}
			// accept new message
			else
			{
				const int bufferSize = DEFAULT_BUFLEN;
				char recvBuffer[DEFAULT_BUFLEN];
				ZeroMemory(recvBuffer, DEFAULT_BUFLEN);
				result = recv(sock, recvBuffer, DEFAULT_BUFLEN, 0);

				// remove client
				if (result <= 0)
				{
					closesocket(sock);
					FD_CLR(sock, &master);
				}
				// send message to other clients
				else
				{
					for (unsigned i = 0; i < master.fd_count; i++)
					{
						SOCKET outSocket = master.fd_array[i];
						if (outSocket != listener && outSocket != sock)
						{
							std::ostringstream ss;
							ss << "SOCKET #" << sock << ": " << recvBuffer << "\n";
							//send(outSocket, recvBuffer, result + 1, 0);
							send(outSocket, recvBuffer, result + 1, 0);
						}
					}
				}
			}
		}
	}

	// cleanup
	WSACleanup();
}
