#include <iostream>
#include <string>
#include <WS2tcpip.h>

#pragma comment (lib, "ws2_32.lib")

const std::string DEFAULT_IP_ADDRESS = "127.0.0.1";
const int DEFAULT_PORT = 54000;
const int DEFAULT_BUFLEN = 4096;

int main()
{
	std::string ipAddress = DEFAULT_IP_ADDRESS;
	int port = DEFAULT_PORT;

	// initialize winsock
	WSAData wsaData;
	WORD ver = MAKEWORD(2, 2);
	int result = WSAStartup(ver, &wsaData);
	if (result != NO_ERROR)
	{
		fprintf(stderr, "Cannot initialize winsock\n");
		return 1;
	}

	// create socket
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET)
	{
		fprintf(stderr, "Cannot create socket: %u\n", WSAGetLastError());
		WSACleanup();
		return 1;
	}

	// define descriptor
	sockaddr_in descriptor; // sockaddr_in is IP-based descriptor
	descriptor.sin_family = AF_INET;
	descriptor.sin_port = htons(DEFAULT_PORT); // network is big endian, Windows is little endian
	inet_pton(AF_INET, ipAddress.c_str(), &descriptor.sin_addr);

	// connect to server
	result = connect(sock, (sockaddr *) &descriptor, sizeof(descriptor));
	if (result == SOCKET_ERROR)
	{
		fprintf(stderr, "Cannot connect to server: %u\n", WSAGetLastError());
		closesocket(sock);
		WSACleanup();
		return 1;
	}
	else
	{
		printf("SERVER> Welcome!\n");
	}

	// send and receive  data
	const int bufferSize = DEFAULT_BUFLEN;
	char recvBuffer[bufferSize];
	std::string userInput;

	do
	{
		// get text
		printf("> ");
		std::getline(std::cin, userInput);

		// send text
		if (userInput.size() > 0)
		{
			result = send(sock, userInput.c_str(), userInput.size() + 1, 0); // size + 1 due to terminating 0
			if (result != SOCKET_ERROR)
			{
				result = recv(sock, recvBuffer, bufferSize, 0);

				// got response
				if (result > 0)
				{
					printf("SERVER> %s\n", std::string(recvBuffer, 0, result).c_str());
				}
			}
		}
	} while (userInput.size() > 0);

	// close
	closesocket(sock);
	WSACleanup();
}
