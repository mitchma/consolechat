
# ConsoleChat #

Playing around with Windows socketing in C++ to create a chatroom in the console. The client only works with the server at the moment (only one client can be connected at a time). The multiclient server can be tested with Putty (or something similar I assume). Just open up the debug exe file in multiclient, and connect using Putty to the localhost with port `54000`.
